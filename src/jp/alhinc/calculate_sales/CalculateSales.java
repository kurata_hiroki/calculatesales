package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	//	支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	//	商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	//	支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	//	商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_CONTINUE = "売上ファイル名が連番になっていません";
	private static final String SALE_FILE_INVALID_FORMAT = "のフォーマットが不正です";
	private static final String SALE_AMOUNT_LIMIT = "合計金額が10桁を超えました";

	private static final String BRANCH_NOT_EXIST = "支店定義ファイルが存在しません";
	private static final String BRANCH_CODE_INVALID_FORMAT = "の支店コードが不正です";
	private static final String BRANCH_FILE_INVALID_FORMAT = "支店定義ファイルのフォーマットが不正です";

	private static final String COMMODITY_NOT_EXIST = "商品定義ファイルが存在しません";
	private static final String COMMODITY_CODE_INVALID_FORMAT = "の商品コードが不正です";
	private static final String COMMODITY_FILE_INVALID_FORMAT = "商品定義ファイルのフォーマットが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//	支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		//	支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		//	商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		//	商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		//	支店定義ファイルの正規表現
		String branchMatch = "^[0-9]{3}$";

		//	商品定義ファイルの正規表現
		String commodityMatch = "^[A-Za-z0-9]{8}$";

		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		//	支店定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, branchMatch, BRANCH_FILE_INVALID_FORMAT, BRANCH_NOT_EXIST)) {
			return;
		}

		//	商品定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, commodityMatch, COMMODITY_FILE_INVALID_FORMAT, COMMODITY_NOT_EXIST)) {
			return;
		}

		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for (int i = 0; i < files.length; i++) {

			if (files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		Collections.sort(rcdFiles);

		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if ((latter - former) != 1) {
				System.out.println(FILE_NOT_CONTINUE);
				return;
			}
		}

		for (int i = 0; i < rcdFiles.size(); i++) {

			BufferedReader br = null;
			List<String> lines = new ArrayList<String>();

			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				String line;
				while ((line = br.readLine()) != null) {
					lines.add(line);
				}

				String fileName = rcdFiles.get(i).getName();

				if (lines.size() != 3) {
					System.out.println(fileName + SALE_FILE_INVALID_FORMAT);
					return;
				}

				if (!branchSales.containsKey(lines.get(0))) {
					System.out.println(fileName + BRANCH_CODE_INVALID_FORMAT);
					return;
				}

				if (!commoditySales.containsKey(lines.get(1))) {
					System.out.println(fileName + COMMODITY_CODE_INVALID_FORMAT);
					return;
				}

				if (!lines.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				Long fileSale = Long.parseLong(lines.get(2));
				Long branchSaleAmount = branchSales.get(lines.get(0)) + fileSale;
				Long commoditySaleAmount = commoditySales.get(lines.get(1)) + fileSale;

				if ((branchSaleAmount >= 10000000000L) || (commoditySaleAmount >= 10000000000L)) {
					System.out.println(SALE_AMOUNT_LIMIT);
					return;
				}

				branchSales.put(lines.get(0), branchSaleAmount);
				commoditySales.put(lines.get(1), commoditySaleAmount);

			} catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		//	支店別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		//	商品別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店または商品コードと支店または商品名を保持するMap
	 * @param 支店または商品コードと売上金額を保持するMap
	 * @param ファイル名の正規表現
	 * @param ファイルのフォーマットに関するエラーメッセージ
	 * @param ファイルの存在に関するエラーメッセージ
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> mapNames,Map<String, Long> mapSales, String match, String invalidFormat, String notExist) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			if (!file.exists()) {
				System.out.println(notExist);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while ((line = br.readLine()) != null) {

				String[] items = line.split(",");

				if ((items.length != 2) || (!items[0].matches(match))) {
					System.out.println(invalidFormat);
					return false;
				}
				mapNames.put(items[0], items[1]);
				mapSales.put(items[0], 0L);
			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> mapNames,Map<String, Long> mapSales) {

		BufferedWriter bw = null;

		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);


			for (String key : mapNames.keySet()) {
				bw.write(key + "," + mapNames.get(key) + "," + mapSales.get(key).toString());
				bw.newLine();
			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}

		return true;
	}
}
